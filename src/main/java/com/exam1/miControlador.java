package com.exam1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author David Mamani C.
 */
@RestController
@RequestMapping("/")
public class miControlador {
    @RequestMapping (method = RequestMethod.GET)
    public void getCustomers(){
        System.out.println("<<<< Hola Mundo >>>>");
    }
}
